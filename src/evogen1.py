#!/usr/bin/env python3

import json
import argparse
import uuid

import evosym

print('evolution/genetics 1')

def create_sim(config_str):
    config = json.loads(config_str)
    genes_cf = config["genes"]
    # print(genes_cf)
    genes = []
    for gene_name, gene_cf in genes_cf.items():
        # for allele_cf in gene_cf
        alleles = []
        for allele_name, allele_cf in gene_cf["alleles"].items():
            alleles.append(evosym.Allele(allele_name, allele_cf["freq"], allele_cf["vit"]))
        dominance = gene_cf["dominance"]
        gene = evosym.Gene(gene_name, alleles, dominance)
        genes.append(gene)
    return evosym.EvoSim(genes, config["population"])

if (__name__ == "__main__"):
    parser = argparse.ArgumentParser(description='run basic evolution simulation')
    parser.add_argument('init', metavar='INITFILE')
    parser.add_argument('-g', '--generations', type=int, help='the number of generations to run', required=True)
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', help='whether to debug to stdout')
    parser.add_argument('-o', '--output', help='the output file', required=True)
    args = parser.parse_args()
    sim = None
    with open(args.init, 'r') as f:
        sim = create_sim(f.read())
    print(sim)
    sim._set_debug_mode(args.debug)
    with open(args.output, 'w') as out:
        out.write(str(uuid.uuid4()) + '\n')
        sim.run(args.generations, out)
