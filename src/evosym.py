# evosym
import random

class Allele:
    def __init__(self, name, frequency, vitality):
        self.name = name
        self.freq = frequency
        self.vit = vitality

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

class Gene:
    def __init__(self, name, alleles, dominance):
        self.name = name
        self.alleles = alleles
        self.dominance = dominance

    def gamete(self):
        v = random.random()
        t = 0
        for allele in self.alleles:
            if v < (t + allele.freq):
                return allele
            t += allele.freq

    def diploid(self):
        return [self.gamete(), self.gamete()]

    def phenotype(self, genotype):
        g = genotype[0]
        for al in genotype:
            if al.name == self.dominance:
                return al
        return g

    def setfreq(self, allele_name, newfreq):
        for al in self.alleles:
            if (al.name == allele_name):
                al.freq = newfreq
                
    def __str__(self):
        r = ''
        for allele in self.alleles:
            r += f" ({allele.name} {allele.freq})s "
        return r

class Organism:
    def __init__(self, genes):
        pass

class EvoSim:
    def __init__(self, genes, population):
        self.genes = genes
        self.pops = population

    def run(self, iterations, out):
        for i in range(iterations):
            # create new genes
            self._debug(f'== Generation {i} ==')
            for gene in self.genes:
                self._debug(f'> Gene {gene.name}')
                raw_allele_counts = {}
                allele_counts = {}
                for allele in gene.alleles:
                    raw_allele_counts[allele.name] = 0
                    allele_counts[allele.name] = 0
                for j in range(self.pops):
                    gt = gene.diploid()
                    ph = gene.phenotype(gt)
                    vit = ph.vit
                    self._debug(f"GT {gt}, PH {ph}, VIT {vit}")
                    for al in gt:
                        raw_allele_counts[al.name] += 1
                        allele_counts[al.name] += 1.0 * vit
                # count new frequencies
                # self._debug('raw allele counts', raw_allele_counts)
                # self._debug('adjusted allele counts', allele_counts)
                
                total_allele_count = sum(allele_counts.values())
                # apply new frequencies
                for new_allele_name, new_allele_count in allele_counts.items():
                    gene.setfreq(new_allele_name, new_allele_count / total_allele_count)
                self._debug(gene)
                
                out.write(f"{gene.name},")
                for allele in gene.alleles:
                    # dump the new ratios
                    out.write(f"{allele.freq},")
                out.write('\n')
                
    def _set_debug_mode(self, v):
        self._debug_mode = v

    def _debug(self, v):
        if self._debug_mode:
            print(v)
